# Debian multiarch pipeline

This repository was forked from [Debian pipeline](https://salsa.debian.org/salsa-ci-team/pipeline). The reason this repository is not hosted on salsa as well is GitLab runners on salsa doesn't support binfmt, which is the very fundamental feature to run foreign-arched binaries with qemu.

## Using Multiarch-Pipeline

### Building

This project provides the definition of build and test jobs.
If want to use this tools to build and test your project, adding the following definitions to your `gitlab-ci.yml` file is the only thing you need to do.

```yaml
include:
  - https://gitlab.com/vicamo/debian-pipeline/raw/master/salsa-ci.yml
  - https://gitlab.com/vicamo/debian-pipeline/raw/master/salsa-ci-simple.yml
```

This will setup an amd64-only pipeline that includes static prerequisite checks, package building, lintian, autopkgtest, reprotest, piuparts, and GitLab Pages based aptly repository for all the built packages all at once. For a real, full multiarch setup, use:

```yaml
include:
  - https://gitlab.com/vicamo/debian-pipeline/raw/master/salsa-ci.yml
  - https://gitlab.com/vicamo/debian-pipeline/raw/master/salsa-ci-native-build.yml
  - https://gitlab.com/vicamo/debian-pipeline/raw/master/salsa-ci-lintian.yml
  - https://gitlab.com/vicamo/debian-pipeline/raw/master/salsa-ci-autopkgtest.yml
  - https://gitlab.com/vicamo/debian-pipeline/raw/master/salsa-ci-reprotest.yml
  - https://gitlab.com/vicamo/debian-pipeline/raw/master/salsa-ci-piuparts.yml
  - https://gitlab.com/vicamo/debian-pipeline/raw/master/salsa-ci-pages.yml
```

This time all architectures official supported by Debian Sid will be included. To switch to cross-compile on non-amd64/i386 architectures, replace `salsa-ci-native-build.yml` with `salsa-ci-cross-build.yml`.

### Skipping Jobs

To skip most architectures except amd64 and arm64 for a quick test, define **SKIP_MOST_ARCHES** to a non-empty string, e.g. 'true'. You may also skip a certian kind of tests by the same trick:

```yaml
include:
  - ...

variables:
  SKIP_MOST_ARCHES: 'true'
  # SKIP_PREREQUISITES:
  # SKIP_CROSS_BUILD:
  # SKIP_AUTOPKGTEST:
  # SKIP_LINTIAN:
  # SKIP_PIUPARTS:
  # SKIP_REPROTEST:
  # SKIP_APTLY:
```

## Contribution

You should always consider upstream your feature to Debian pipeline whenever appropriate as this project will merge its HEAD. Anyway, contribution is still welcome and appriciated.
